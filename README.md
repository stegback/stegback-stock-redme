# Stock Management Package

This  package provides stock management features, allowing you to manage stock quantity, sell products, and handle orders. It includes a multi-company admin panel for efficient management.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [License](#license)

## Features

### Multi-Company Support

- Manage stock and orders for multiple companies efficiently.

### Stock Quantity Management

- Track and update stock quantities for different products.

### Order Management

- Handle customer orders, process payments, and manage order fulfillment.

### Admin Panel

- User-friendly admin panel for easy navigation and management.

### Authentication and Authorization

- Secure access with authentication and authorization features.

## Installation

### Database 

- [stegbacks](#stegbacks)
- [other_skus](#other_sku)
- [sku_relation](#sku_relation)
- [stock_reduces_other_stegback](#stock_reduces_other_stegback)
- [create_col](#create_col)


#### stegbacks
```sql
    CREATE TABLE `stegbacks` (
        `id` int(11) NOT NULL,
        `name` varchar(50) DEFAULT NULL,
        `cmp_name` varchar(100) DEFAULT NULL,
        `url` varchar(100) DEFAULT NULL,
        `get_fun` varchar(100) DEFAULT NULL,
        `add_fun` varchar(100) DEFAULT NULL,
        `secret_key` varchar(255) DEFAULT NULL,
        `active_status` tinyint(1) DEFAULT 1 COMMENT '1 active , 2 deactive',
        `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
        `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
```
   

#### other_sku
```sql
    CREATE TABLE `other_skus` (
        `id` int(11) NOT NULL,
        `good_id` int(11) DEFAULT NULL,
        `steg_id` int(11) DEFAULT NULL,
        `parent_sku` varchar(250) DEFAULT NULL,
        `sku` varchar(250) DEFAULT NULL,
        `quantity` int(11) DEFAULT NULL,
        `await_qty` int(11) DEFAULT NULL,
        `status` tinyint(1) DEFAULT 0 COMMENT '1 checked , 2 unchecked , 0 nothing for stegback channel checked or not ',
        `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
        `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
```

#### sku_relation
``` sql 
    CREATE TABLE `sku_relation` (
        `id` int(11) NOT NULL,
        `sku` varchar(55) NOT NULL,
        `parent_sku` varchar(55) NOT NULL,
        `status` tinyint(1) NOT NULL,
        `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
        `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
```
#### stock_reduces_other_stegback

``` sql 
    CREATE TABLE `stock_reduces_other_stegback` (
        `id` int(11) NOT NULL,
        `order_id` varchar(250) DEFAULT NULL,
        `parent_sku_id` int(11) DEFAULT NULL COMMENT 'order_skus table id',
        `product_id` varchar(255) DEFAULT NULL,
        `product_sku` varchar(255) DEFAULT NULL,
        `qty` int(11) DEFAULT NULL,
        `stock_before` int(11) DEFAULT NULL,
        `stock_after` int(11) DEFAULT NULL,
        `stock_after_wh` int(11) DEFAULT 0,
        `order_product_id` int(11) DEFAULT 0,
        `type` varchar(255) NOT NULL DEFAULT 'reduce',
        `user_name` varchar(255) DEFAULT '',
        `stegback_id` int(11) DEFAULT NULL,
        `parent_sku_reduce_id` int(11) DEFAULT NULL,
        `created_at` datetime DEFAULT NULL,
        `updated_at` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
```
#### create_cols

**Add ware_status and stegback_id columns to order_skus table**
``` sql 
    ALTER TABLE order_skus
    ADD COLUMN ware_status BOOLEAN DEFAULT 0,
    ADD COLUMN stegback_id INT(11) NULL;
```

**Add stegback_id column to wh_stock_reduce table**
``` sql 
    ALTER TABLE wh_stock_reduce
    ADD COLUMN stegback_id INT(11) NULL;
```

**Add stegback_id column to stock_reduce table**
``` sql 
    ALTER TABLE stock_reduce
    ADD COLUMN stegback_id INT(11) NULL;
```

###  Files

- [controller](#controller)
- [observer](#observer)
- [modals](#modal)
- [job](#job)
- [middleware](#middleware)
- [resources](#resources)

#### controller
1. Create `StegbackController.php`.
2. Create `StockManagementController.php`.

#### observer
1. Create `StockReduceObserver.php`.

**Note:** Register observer `EventServiceProvider.php`.

#### modals
1. Create `OtherSku.php`.
2. Create `Stegback.php`.
3. Create `SkuRelation.php`.
4. Create `StockReducesOther.php`.

#### job
1. Create `StockReduceJob.php`.

#### middleware
1. Create `SecretKey.php`.

** Note: ** Register middleware `RouteServiceProvider.php` and `kernal.php`. 


### Routes

**Route's in `api.php` file**
1. ```php 
    Route::post('update-qty-from-main',[StegbackController::class , 'update_qty_from_main']);
    ```
2. ```php 
    Route::post('update-sku-relation-data',[StegbackController::class , 'change_status_sku_relation']);
    ```
3. ```php 
    Route::post('update-reduce-data',[StegbackController::class , 'update_reduce_data']);
    ```
4. ``` php 
    Route::post('other-stegback-qty-reduce',[StegbackController::class , 'update_qty']);
    ```
5. ```php 
    Route::post('stock_reduce_other_stegbacks',[StockManageController::class , 'stock_reduce_other_stegbacks']);
    ```
6. ```php 
    Route::post('view_stegback/{name}', [StegbackController::class , 'view']);
    ```
7. ```php
    Route::post('get_sku/{sku}', [StegbackController::class , 'sku']);
    ```

8. ```php
    Route::any('get-order-bill-data_other/{steg_id}',[StegbackController::class , 'get_order_bill_data_other']);
    ```

**Route's in `web.php` file**

1. ```php
    Route::any('save_sku', [StegbackController::class, 'save_sku']);
    ```

2. ```php
    Route::any('view-connected-sku', [StegbackController::class, 'view_connected_sku']);
    ```    

 ### resources

 1. Create `view_connected_sku` file for display count of other stegback orders.

### Some Updates

1. Add in `.env` file
    ``` php
        X_SECRET_KEY=stegbackCom
        SSU=https://service.stegback.dk/api/
        SERVICE_IP = 199.250.202.1
    ```

2. Add in `config\app.php` file
    ```php
        'service_ip' => env('SERVICE_IP','199.250.202.1'),
    ```    

3. Add in `controller.php` file
    ```php 
        /**
         * Create stock reduce entry with the quantity of connected sku
        */
        
        public function get_other($skuData, $order, $qty,$orderSkuId = NULL,$sku =''):bool
        {
            if ($sku == '' && !isset($skuData['sku'])) {
                return false;
            }
        
            // Assign $sku from $skuData['sku'] if it exists, otherwise use $sku
            $sku = @$skuData['sku'] ?: $sku;
            
            $order_reduce = new StockReduce;
            $order_reduce->order_id = @$order['order_id'];
            $order_reduce->product_id = @$order['product_id'];
            $order_reduce->parent_sku_id = @$orderSkuId;
            $order_reduce->order_product_id = @$order['id'];
            $order_reduce->product_sku  = $sku;
            $order_reduce->qty = $qty;
            $order_reduce->stock_before = $skuData->other_sku['await_qty'] ?: $skuData->other_sku['quantity'];
            $order_reduce->stock_after =  $skuData->other_sku['await_qty'] - $qty;
            $order_reduce->stegback_id =  $skuData->other_sku['steg_id'] ;
            if($order_reduce->save())
            {
                OtherSku::where('sku', $skuData['sku'])->update(['await_qty' => $order_reduce->stock_after]);
            }
            return true;
        }

        /**
         * Check the relation count for a given SKU in the SkuRelation table.
         *
         * @param  string  $sku
         * @return int
         */
        public function relation_count($sku): int
        {
            return SkuRelation::where('status', 1)->where('sku', $sku)->count();
        }

        public function update_sku_connected_channel($sku,$qty)
        {
            
            $headers = [
                'Content-Type' => 'application/json',
                'Origin' => url('/'),
                'X-Secret-Key'=> env('self_secret'),
            ];
            $response = Http::withHeaders($headers)->post(env('SSU').'update-qty-from-main',['sku' => $sku, 'qty' => $qty]);   
            
        }

        public function remove_other($stockReduce,$quantity,$stockReduceOrderId,$type)
        {
            // print_r($stockReduce->other_sku['sku']);die;
            $otherSku = OtherSku::where('sku', $stockReduce->other_sku['sku'])->first();
            
            $order_reduce = new StockReduce;
            $order_reduce->order_id = $stockReduceOrderId;
            $order_reduce->product_id = '';
            $order_reduce->product_sku  = $otherSku['parent_sku'];
            $order_reduce->qty = $quantity;
            $order_reduce->stock_before = @$otherSku->await_qty ?? 0; 
            if($type == 'revert')
            {
                $order_reduce->stock_after = @$otherSku->await_qty + $quantity;
                $order_reduce->type = 'increase';
                
            }else {
                $order_reduce->stock_after = @$otherSku->await_qty - $quantity;
                $order_reduce->type = 'reduce';
            }
            
            $order_reduce->stock_after_wh = @$otherSku->await_qty ?? 0;
            $order_reduce->user_name = auth()->user()->name;
            $order_reduce->save();
            
            $otherSku->await_qty = $otherSku->await_qty + $quantity;
            $otherSku->save();
            
            return true;
        } 
    ```
-- Note: customize varibale and data as per your requirement and don't forget to include or use related model's , controller which are use in function's

4. Create relation in `Models\Sku.php` file.
    ``` php 
         public function other_sku()
        {
            return $this->belongsTo('App\Models\OtherSku', 'parent_sku', 'sku')->where('status',1);
        }
    ```
5. Add to the CronController.php file:
    -- Get other_sku table data with SKU table for group_sku_reduce and variable_single_sku_reduce.
    -- Note: Customize variables and data as per your requirements and don't forget to include or use related models and controllers used in the functions.

6. Add to the TestController.php file:
    -- Get other_sku table data with SKU table for revert_stock_new, and reduce_mannual_qty_sku.
    -- Call remove_other function for increased and reduced stock entries when SKUs are connected to other company SKUs, which are primary SKUs.
    -- Call relation_count function to check if self-SKUs are connected to other company SKUs where self-SKUs are primary SKUs. If connected, then call update_sku_connected_channel to make a request to the main server, which updates the latest quantity for other connected companies.
    -- Note: Customize variables and data as per your requirements and don't forget to include or use related models and controllers used in the functions.

7. Add a WHERE condition ware_stat = 0 to check if the stock is reduced from the main company. It will not reduce from our company's warehouse to which  function reduces warehouse quantity from   your company's warehouse. Mainly functions are available in OrderController.php and HomeController.php.
    -- Note: Customize variables and data as per your requirements and don't forget to include or use related models and controllers used in the functions.

## License

This Stock Management Package is Stegpearl licensed under the [MIT License](LICENSE.md).